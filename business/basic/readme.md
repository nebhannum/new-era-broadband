---
title: Basic Business Plans at New Era Broadband
meta:
- name: description
  content: New Era Broadband business services.

---
<h1>Basic Business Plans</h1>

<h2>Priced by Service Level</h2>
<div class="container mt-4 mb-3">
  <div class="row">
    <div class="col-12 col-md-4">
      <h3>6MB Download x 1MB Upload Internet Speed</h3>
      <ul>
        <li>Managed Router and Public IP Address</li>
        <li>$99.99/mo.</li>
      </ul>
    </div>
    <div class="col-12 col-md-4">
      <h3>10MB Download x 2MB Upload Internet Speed</h3>
      <ul>
        <li>Managed Router and Public IP Address</li>
        <li>$129.99/mo.</li>
      </ul>
    </div>
    <div class="col-12 col-md-4">
      <h3>15MB Download x 3MB Upload Internet Speed</h3>
      <ul>
        <li>Managed Router and Public IP Address</li>
        <li>$159.99/mo.</li>
      </ul>
    </div>
  </div>
</div>

<div class="text-center"><h2>Get Started with Our Basic Services Today!</h2></div>

<div class="text-center mt-3 mb-4"><a href="https://www.newerabroadband.com/#contact" class="btn btn-primary">Contact Us Now</a></div>

<h2>Symmetric Services available – custom developed for your needs.</h2>

*Installation fees & first month service fees due at time of install.
See Terms of Service for information on monthly billing cycles and options. Business installation only $399.99. 

<img src="https://www.newerabroadband.com/business-owner.jpg" alt="" class="mt-2">