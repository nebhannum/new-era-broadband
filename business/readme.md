---
title: Business Digital Connectivity Services for Business
meta:
  - name: description
    content: 'Check out what New Era Broadband services has to offer for your business needs.'
---

<div class="container">
  <div class="row">
    <div class="col-12"><h1>Business Services</h1></div>
  </div>
  <div class="row">
    <div class="col-12 col-md-4">
      <h3>Basic Business Service</h3>
      <p>Priced by Service Level</p>
      <ul>
        <li>6MB Download x 1MB Upload Internet Speed</li>
        <li>Managed Router and Public IP Address</li>
        <li>$99.99/mo.</li>
      </ul>
      <ul>
        <li>10MB Download x 2MB Upload Internet Speed</li>
        <li>Managed Router and Public IP Address</li>
        <li>$129.99/mo.</li>
      </ul>
      <ul>
        <li>15MB Download x 3MB Upload Internet Speed</li>
        <li>Managed Router and Public IP Address</li>
        <li>$159.99/mo.</li>
      </ul>
      <p>Symmetric Services available – custom developed for your needs.</p>
    </div>
    <div class="col-12 col-md-4">
      <h3>Metered Business Service</h3>
      <em>High Speed Pay By Useage</em>
      <ul>
        <li>10MB Aggregate Internet Speed</li>
        <li>Public IP Address and Managed Router</li>
        <li>$59.99/mo. gives you 50GB Traffic</li>
        <li>$0.50/GB after that.</li>
      </ul>
      <ul>
        <li>25MB Aggregate Internet Speed</li>
        <li>Public IP Address and Managed Router</li>
        <li>$79.99/mo. gives you 75GB Traffic</li>
        <li>$0.50/GB after that.</li>
      </ul>
      <ul>
        <li>50MB Aggregate Internet Speed</li>
        <li>Public IP Address and Managed Router</li>
        <li>$99.99/mo. gives you 100GB Traffic</li>
        <li>$0.50/GB after that.</li>
      </ul>
    </div>
    <div class="col-12 col-md-4">
      <h3>VoIP Phone System</h3>
      <em>$Price on Consultation*</em>
      <p>Digital (VoIP) Phone System. Multiple Numbers, Multiple Lines, Multiple Handsets</p>
      <ul>
        <li>Unlimited Local and Long Distance</li>
        <li>Multiple Voice Mail Messages</li>
        <li>Multiple Lines – Programmable</li>
        <li>Managed Call Forwarding & Call From Anywhere</li>
        <li>Conference Calling</li>
        <li>Video Capability Available</li>
      </ul>
    </div>
  </div>
  <div class="row">
    <div class="col-12">*Installation fees & first month service fees due at time of install. See Terms of Service for information on monthly billing cycles and options. Business installation only $399.99.</div>
  </div>
</div>
