---
title: Cyber Security at New Era Broadband
meta:
- name: description
  content: New Era Broadband business services.

---
# Cyber Security

## Technical Overview

<div class="container">
<div class="row">
<div class="col-4">
<div><i class="fa fa-4x fa-cloud" aria-hidden="true"></i></div>
<p>PC Matic Pro uses a cloud-based, global whitelist to protect Windows endpoints. Unknown applications are always blocked, which allows us to block even the newest forms of malware. In contrast to traditional application control, PC Matic Pro won’t require extra IT staff for implementation. Our whitelist is updated with more known good software everyday, providing application whitelisting with no heavy lifting for IT departments.</p>
</div>
<div class="col-4">
<div><i class="fa fa-4x fa-shield" aria-hidden="true"></i></div>
<p>SuperShield, a component of PC Matic, hooks each running process and will check the static information of the application before it is allowed to run. This check is performed quickly and uses little system resources because there is no behavioral analysis involved. PC Matic will return a result of either known good (allowed to run), known bad (will not execute), or unknown (blocked & uploaded for analysis by our research team).</p>
</div>
<div class="col-4">
<div><i class="fa fa-4x fa-list" aria-hidden="true"></i></div>
<p>Our global whitelist has been curated for years by our malware team allowing easy implementation of application whitelisting with a simple client install. After installing, the web portal allows for access to vulnerability management, driver management, and maintenance, all requiring only the initial endpoint client installation. Additionally, Remote Desktop Protocol is available from the same web portal, allowing 1-click access to endpoints.</p>
</div>
</div>
</div>

<div class="container">
<div class="row">
<div class="col-12 mt-3 text-center">
<img src="https://www.newerabroadband.com/PCMaticPro.jpg" alt="">
</div>
</div>
</div>

## Whitelisting is backed by 3rd parties

### Forrester

"Whitelisting uses a "positive security" model, which defines what is "good and allowed" and rejects everything else. In contrast, a blacklist-based security model defines what should be rejected and implicitly allows everything else. A positive security model is powerful because it: Defaults to deny and Is attack-agnostic"

_- Chris Sherman and Chenxi Wang, Ph.D; Forrester Analysts_

### Gartner

Analysts Peter Firstbrook and Neil MacDonald believe Whitelisting solutions: "...can provide excellent proactive protection by enforcing a "default-deny" application execution environment, allowing only known applications to execute"

Moreover, none of the signature-based malware engines are ever 100% effective at detecting known threats, and accuracy at detecting new threats is only 30%. Low distribution/targeted threats are even more elusive to signature techniques.