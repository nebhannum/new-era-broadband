---
title: Metered Business Services at New Era Broadband
meta:
- name: description
  content: New Era Broadband business services.

---
# Metered Business Services

## High Speed Pay By Useage

<div class="container mt-4 mb-3">
  <div class="row">
    <div class="col-12 col-md-4">
      <h3>10MB Aggregate Internet Speed</h3>
      <ul>
        <li>Public IP Address and Managed Router</li>
        <li>$59.99/mo. gives you 50GB Traffic</li>
        <li>$0.50/GB after that.</li>
      </ul>
    </div>
    <div class="col-12 col-md-4">
      <h3>25MB Aggregate Internet Speed</h3>
      <ul>
        <li>Public IP Address and Managed Router</li>
        <li>$79.99/mo. gives you 75GB Traffic</li>
        <li>$0.50/GB after that.</li>
      </ul>
    </div>
    <div class="col-12 col-md-4">
      <h3>50MB Aggregate Internet Speed</h3>
      <ul>
        <li>Public IP Address and Managed Router</li>
        <li>$99.99/mo. gives you 100GB Traffic</li>
        <li>$0.50/GB after that.</li>
      </ul>
    </div>
  </div>
</div>

<div class="text-center"><h2>Get Started with Our Metered Services Today!</h2></div>

<div class="text-center mt-3 mb-4"><a href="https://www.newerabroadband.com/#contact" class="btn btn-primary">Contact Us Now</a></div>

*<b>Installation fees & first month service fees due at time of install.</b>  
See Terms of Service for information on monthly billing cycles and options. Business installation only                     $399.99.

<div class="text-center"><img src="https://www.newerabroadband.com/business-2.jpg" style="margin-bottom: 30px;" alt=""></div>