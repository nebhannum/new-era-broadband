---
title: Business Digital Phone Services at New Era Broadband
meta:
- name: description
  content: New Era Broadband business services.

---
# Digital Phone Services for Business

<div class="container">
  <div class="row">
    <div class="col-12 col-md-6">
      <p>Service starts at $ 19.95 per month </p>
      <p>Each Ooma Office account includes a local or toll-free phone number, a virtual fax extension, and a conference extension. Need more? Supports up to 20 users. Each user is only $ 19.95/month and includes a phone number.</p>
      <p>Compare Ooma Office features Calling  Unlimited calling in U.S. and Canada  Low international rates Setup  Easy self installation  Needs no wiring  24/7 customer support Office features  Virtual receptionist  Modes for business and after hours  Caller-ID  E911 service  Extension dialing  Internet fax (forwards to email)5  Voicemail  Voicemail forwarding to email  Call transfer  Ring groups  Music-on-hold  Conference server5  Online call logs Mobility features  Mobile App  Call forwarding  Multi-ring</p>
    </div>
    <div class="col-12 col-md-6"><img src="https://www.newerabroadband.com/no-contracts-ever.jpg" alt=""></div>
  </div>
</div>

<div class="container">
  <div class="row">
    <ul class="col-12 col-md-4">
      <h2>Calling Features</h2>
      <li>Unlimited calling in U.S., Canada, Mexico and Puerto Rico</li>
      <li>Low international rates</li>
      <li>911 service</li>
      <li>FREE number transfer</li>
      <li>One FREE toll-free number (includes 500 minutes of inbound calls per month)</li>
      <li>Main line company number</li>
      <li>One direct dial number per user</li>
      <li>Setup</li>
      <li>Easy installation</li>
      <li>Wireless options available</li>
      <li>Ooma Office IP phones</li>
      <li>Ooma Office mobile app</li>
      <li>Analog phones</li>
      <li>User portal</li>
      <li>Fax machine port (with Base Station)</li>
      <li>24/7 customer support</li>
    </ul>
    <ul class="col-12 col-md-4">
      <h2>Office Features</h2>
      <li>Virtual receptionist options:</li>
      <li>Multi-level</li>
      <li>Business modes for open and closed hours</li>
      <li>Dial by name</li>
      <li>Route callers</li>
      <li>Custom messages</li>
      <li>Call park</li>
      <li>Ring groups</li>
      <li>Music on hold</li>
      <li>Transfer music</li>
      <li>Extension dialing</li>
      <li>Voicemail</li>
      <li>Call transfer</li>
      <li>Call log</li>
      <li>One conference bridge per user</li>
      <li>Extension-to-extension dialing</li>
      <li>Forward calls during device outages</li>
      <li>One virtual fax per user</li>
      <li>7-digit dialing</li>
    </ul>
    <ul class="col-12 col-md-4">
      <h2>Mobility Features</h2>
      <li>Smart phone app: 2-phones-in-1</li>
      <li>Call forwarding</li>
      <li>Multi-device ring</li>
      <li>Virtual extensions</li>
      <li>Voicemail audio email attachments</li>
    </ul>
  </div>
</div>

<img src="https://www.newerabroadband.com/business-banner.jpg" alt="">