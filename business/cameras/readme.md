---
title: Video Surveillance at New Era Broadband
meta:
  - name: description
    content: 'New Era Broadband business services.'
---
# Video Surveillance

New Era Broadband can outfit your business with scalable enterprise-grade video equipment.  Sold and serviced locally by our trained technicians.

<div class="text-center"><h2>Get Started with Video Surveillance Today!</h2></div>

<div class="text-center mt-2"><a href="https://www.newerabroadband.com/#contact" class="btn btn-primary">Contact Us Now</a></div>

<div class="mt-3 text-center"><img src="https://www.newerabroadband.com/video1.PNG" alt=""></div>