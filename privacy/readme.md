---
title: New Era Broadband Privacy Policy
meta:
  - name: description
    content: 'Read the New Era Broadband privacy policy.'
---
# Privacy Statement

## We are committed to respecting our customer's privacy.

Once you choose to provide us with personally identifiable information, it will only be used in the context of your customer relationship with us. We will use this information to market our products and services to you, but we will not disclose, sell, rent, or lease your personally identifiable information to others. Unless required by law or your prior permission is obtained, we will only share the personal data you provide with other New Era Broadband Services entities and/or business partners who are acting on our behalf to provide you the services you have requestd. Such Vox entities and/or national or international business partners are governed by our privacy policies with respect to the use of this data.

We will not read, listen to or disclose to any third parties private conversations or other communications that are transmitted using our services except as required to ensure proper operation of services or as otherwise authorized by law.

We will do its best to honor requests from customers for account information, for example, name, address, company, or billing information. The customer is responsible for ensuring that the information on file with Vox is current and accurate.

We do not sell services or equipment to children. We do not knowingly solicit services from people who are under the age of eighteen. In addition, we will not knowingly provide a link to any third party web site that solicits or collects personally identifiable information from minors.

Our VoIP provider is a wholly owned subsidiary of a publicly traded company, and may be required to file numerous reports with different administrative bodies. Consequently, they may provide aggregate statistics about customers, sales and geographical concentrations of traffic. None of these reports or statistics will include personally identifiable information. However, they reserve the right to use personally identifiable information to investigate and help prevent potentially unlawful activity that threatens either them or any company affiliated with them. Moreover, upon the appropriate request of a government agency, law enforcement agency, court or as otherwise required by law, they may disclose personally identifiable information.