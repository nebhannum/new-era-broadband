---
title: New Era Broadband Customer and Technical Support
meta:
- name: description
  content: Receive 24/7 technical and customer support for New Era Broadband services.

---
## Customer Support 9a.m. - 5p.m.

| Day  | Monday        | Tuesday       | Wednesday     | Thursday      | Friday        |
|------|---------------|---------------|---------------|---------------|---------------|
| Time | 9a.m. - 5p.m. | 9a.m. - 5p.m. | 9a.m. - 5p.m. | 9a.m. - 5p.m. | 9a.m. - 5p.m. |
Receive courteous personal Customer Support for billing and account questions, during our regular business hours (9am – 5pm weekdays excluding holidays). When we’re not available, our friendly answering service will be happy to take your calls!

Call: [(740) 992-0620](tel:7409920620) (24/7)

## Technical Support

| Day  | Monday        | Tuesday       | Wednesday     | Thursday      | Friday        |
|------|---------------|---------------|---------------|---------------|---------------|
| Time | 9a.m. - 5p.m. | 9a.m. - 5p.m. | 9a.m. - 5p.m. | 9a.m. - 5p.m. | 9a.m. - 5p.m. |

Technical Support by phone is available Mon thru Fri 9am-5pm (except holidays). When we are not there, our phones are monitored 24/7. If you are having trouble with your New Era Broadband connection, our professional staff will assist you as quickly as possible. Late evening and weekend calls will be responded to the next business day! If it is an emergency, explain in your messages and we’ll get back with you as soon as possible.

Call: [(740) 992-0620](tel:7409920620) (24/7)