---
title: Digital Home Phone Services at New Era Broadband
meta:
- name: description
  content: New Era Broadband home phone services.

---
# Ooma Premier Digital Home Phone Services

![](https://www.newerabroadband.com/telo-shot.png)

<div class="text-center mt-4"><h2>All these features for only $14.99/mo.</h2></div>

<div class="text-center mt-2 mb-4"><a href="https://www.newerabroadband.com/#contact" class="btn btn-primary">Contact Us Now</a></div>

<div class="container">
<div class="row">
<ul class="col-12 col-md-6">
<li><h2>Instant Second Line</h2>
<p>Two lines are better than one. Make or receive a second call even while your phone is in use.</p></li>
<li><h2>Free Calling to Canada and Mexico</h2>
<p>Got family and friends in Canada and Mexico? Now you can talk to your heart’s content. As a Premier subscriber, calls to Canada and Mexico are free (excluding the 867 calling area).</p></li>
<li><h2>Backup Number</h2>
<p>If your internet goes down, calls are automatically forwarded to another phone number.</p></li>
<li><h2>Three-Way Conferencing</h2>
<p>Whether you’re doing business or chatting with friends, three-way conference calling has never been easier.</p></li>
<li><h2>Voicemail Monitoring</h2>
<p>If you don’t recognize the caller-ID, listen as they leave a message and pick up the phone at any time to speak with the caller.</p></li>
<li><h2>Send to Voicemail</h2>
<p>Transfer a call to voicemail by pressing the “Send to Voicemail” button at any time.</p></li>
<li><h2>Private Voicemail</h2>
<p>Make any Ooma HD2 Handset or Ooma Linx a private extension with a password-protected voicemail account. You can also set up personal greetings, customize privacy settings, and control voicemail notifications or forwarding.</p></li>
<li><h2>Caller Name</h2>
<p>See the name of callers, even if they’re not in your address book.</p></li>
<li><h2>Expanded Call Blocking</h2>
<p>Select from three pre-configured protection settings, or build your own customized call blocking profile.</p></li>
</ul>
<ul class="col-12 col-md-6">
<li><h2>Google Voice Extensions</h2>
<p>Experience Google Voice on your Ooma system. We’ve simplified and enhanced the experience so that you can access Google Voice’s Voicemail, Call Presentation, Listen In, and Caller-ID features, all with the press of a button.</p></li>
<li><h2>Call Forwarding</h2>
<p>Forward your calls to any number, even your cell phone, so that you never miss a call.</p></li>
<li><h2>Do Not Disturb</h2>
<p>Get peace and quiet whenever you want by simply pressing the envelope icon for two seconds. All of your calls will roll into voicemail without ringing.</p></li>
<li><h2>Free Mobile Minutes</h2>
<p>Ooma Premier customers using the Ooma Mobile App can make and receive calls anywhere in the U.S. for no charge.</p></li>
<li><h2>Multi-Ring</h2>
<p>Ensure you never miss a call. Configure your Ooma system to simultaneously ring your mobile phone and home phone.</p></li>
<li><h2>Voicemail-to-Email Forwarding (Audio)</h2>
<p>Enjoy the ultimate convenience of having your voicemail messages forwarded to your email as an audio file. Now you can listen to messages anywhere you can check email.</p></li>
<li><h2>Voicemail Alerts</h2>
<p>Keep up to date with new message notifications that can be sent to any email address or SMS-capable mobile phone.</p></li>
<li><h2>Private Device</h2>
<p>Set aside one or more of your phones as a dedicated line that will only ring when a particular phone number is dialed. Give anyone in the house their own telephone, set up a dedicated fax line, or separate your home phone and work phone.</p></li>
</ul>
</div>
</div>