module.exports = {
  title: 'New Era Broadband Services',
  description: '',
  base: '/',
  dest: './public',
  themeConfig: {
    logo: '/new_era_broadband_logo.svg',
    // sidebar: [
    //   { title: 'Home', link: '/' },
    //   { title: 'Internet', link: '/#internet' },
    //   { title: 'Entertainment', link: '/#entertainment' },
    //   { title: 'Phone', link: '/#phone' },
    //   { title: 'Business', link: '/#business' },
    //   { title: 'Contact', link: '/#contact' },
    //   { title: 'Phone', link: '/#phone' },
    // ],
    head: [
      ['link', { rel: 'icon', href: '/logo.png' }]
    ],
    nav: [
      { text: 'Internet', link: '/#internet' },
      { text: 'Business', link: '/#business' },
      { text: 'Digital Connections', link: '/#connections' },
      { text: 'Testimonials', link: '/#testimonials' },
      { text: 'Contact', link: '/#contact' },
      { text: 'About', link: '/#about' },
      { text: 'Login', items: [
        {text: 'Account Portal', link: 'https://portal.mynewera.net/'},
        {text: 'Phone Portal', link: 'https://my.ooma.com/'},
        {text: 'Email', link: 'https://mail01.ori.net/Login.aspx'}
      ] }
    ]
  }
}