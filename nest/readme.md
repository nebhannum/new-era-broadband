---
title: Nest at New Era Broadband
meta:
- name: description
  content: Nest provided by New Era Broadband.

---
# Nest

![Kitchen countertop with Google Home on a tablet and other utincils](https://www.newerabroadband.com/bg-desktop-3903501336.jpg "Home automation with Nest")

Home. It's more than just four walls and a roof over your head.   
It's where you feel safest and most comfortable.   
But what if your home knew you as well as you know it?   
What if it could recognize you, and anticipate your needs?

Or even better, what if it took care of the things you forget.   
Turned down the heat. Turned off the lights. Locked the door.   
What if your home became – in small ways,   
then big ones – an extension of you?

<div class="text-center"><h2>Get Started with Nest Security Today!</h2></div>

<div class="text-center mt-2 mb-5"><a href="https://www.newerabroadband.com/#contact" class="btn btn-primary">Contact Us Now</a></div>

![Two nest thermostats: one white one black](https://www.newerabroadband.com/nest-thermostat.png "Nest Thermostat")

*It's beautifully designed to keep you comfortable and help save energy.*

These aren't just daydreams or someday's.   
It's what we're passionately building at Nest, everyday.

Our mission is to create a home that takes care   
of the people inside it and the world around it.   
Because really if you think about it,   
the world is just one big neighborhood.

<div class="text-center"><h2>Get Started with Nest Security Today!</h2></div>

<div class="text-center mt-2"><a href="https://www.newerabroadband.com/#contact" class="btn btn-primary">Contact Us Now</a></div>

![Nest security cameras](https://www.newerabroadband.com/nest-cameras.png "Nest security cameras")

*Every Nest Cam plugs into power so it won't miss a second, can send helpful notifications to your phone, and is designed with an all-glass lens and premium materials.*