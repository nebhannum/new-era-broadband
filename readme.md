---
hero: linda-xu-39084-unsplash.jpg
heroText:
  title: Connecting Meigs County, Ohio and Beyond
  text: 'Now serving the communities of: Albany, Danville, Five Points, Harrisonville,
    Hemlock Grove, Letart Falls, Long Bottom, Pageville, Pomeroy, Racine, Reedsville,
    Rocksprings, Shade, Tuppers Plains, and Wilkesville, Ohio.'
  callToActionText: Get Started
  callToActionLink: "./check/"
home: true
title: New Era Broadband - Internet, Phone, and Entertainment Services
meta:
- name: description
  content: View internet, phone, and entertainment services available to areas in
    Southeastern Ohio.

---
<div class="pt-4 pb-5">
<div class="container">
<div class="row">
<a id="internet" class="anchor"></a>
<div class="col-12 mb-4 text-left"><h2><i class="fa fa-desktop" aria-hidden="true"></i> Internet Services</h2></div>
<div class="col-12 col-md-4 mb-2 mb-md-0">
<div class="packages-background card">
<div class="card-body">
<h3 class="card-title">Turbo</h3>
<p class="card-text">$64.99/mo*</p>
<p class="card-text">4 MB Service Great For:</p>
<ul class="card-text card-details">
<li>Watching SD video</li>
<li>Skype and over VoIP Phone</li>
<li>Work from home and taking online courses</li>
</ul>
</div>
</div>
</div>
<div class="col-12 col-md-4 mb-2 mb-md-0">
<div class="packages-background card">
<div class="card-body">
<h3 class="card-title">Nitro</h3>
<p class="card-text">$77.99/mo*</p>
<p class="card-text">7 MB Service Great For</p>
<ul class="card-text card-details">
<li>Online Gaming</li>
<li>Multiple Device SD Streaming</li>
<li>Single User HD Streaming</li>
</ul>
</div>
</div>
</div>
<div class="col-12 col-md-4 mb-2 mb-md-0">
<div class="packages-background card">
<div class="card-body">
<h3 class="card-title">Mach 1</h3>
<p class="card-text">$99.99/mo.**</p>
<p class="card-text">10 MB Service Great For:</p>
<ul class="card-text card-details">
<li>Hard Core Gamers</li>
<li>Multiple HD Streaming</li>
<li>Video Conferencing</li>
</ul>
</div>
</div>
</div>
</div>
<div class="row mt-0 mt-md-4">
<div class="col-12 col-md-4 mb-2 mb-md-0">
<div class="packages-background card">
<div class="card-body">
<h3 class="card-title">Satellite</h3>
<p class="card-text">Starting at $49.99/mo.**_</p>
<p class="card-text">12 MB Service Great For:</p>
<ul class="card-text card-details">
<li>Surfing the Web</li>
<li>Watching Short Videos</li>
<li>Online Shopping</li>
</ul>
</div>
</div>
</div>
<div class="col-12 col-md-4 mb-2 mb-md-0">
<div class="packages-background card">
<div class="card-body">
<h3 class="card-title">Digital Phone</h3>
<p class="card-text">$14.99/mo_</p>
<p class="card-text">Premier Service:</p>
<ul class="card-text card-details">
<li>Unlimited Calling</li>
<li>Voice Mail</li>
<li>Caller Id and Much More</li>
<li><a href="./phone/" style="color: white;">Learn More</a></li>
</ul>
</div>
</div>
</div>
<div class="col-12 col-md-4 mb-2 mb-md-0">
<div class="packages-background card">
<div class="card-body">
<h3 class="card-title">And More!</h3>
<p class="card-text">Call to see what other plans we have available to offer for your needs.</p>
</div>
</div>
</div>
</div>
<div class="row mt-3">
<div class="col-12">
<p>* No Service Commitment. Installation fees & pro-rated first month service fees due at time of install. See Terms of Service for information on monthly billing cycles and options. Installation only $199.99 (Installation fee can be broken up into 4 monthly payments of $49.99).</p>
<p>** Not available in all locations. Check with us for availability in your area.</p>
<p>*** Speeds of satellite plans vary based upon plan usage and times of day. Check details for more information</p>
<p></p>All plans are dependent upon a location inspection and analysis and may be subject to change. Plan availability dependent upon tower location.</p>
<div class="text-center"><a href="./check/" class="btn btn-secondary">Check Coverage</a></div>
</div>
</div>
</div>
</div>

<div class="packages-background pt-4 pb-5"> <div class="container"> <div class="row mt-3"> <a id="business" class="anchor"></a> <div class="col-12 text-left mb-4"><h2><i class="fa fa-briefcase" aria-hidden="true"></i> Business Services</h2></div> </div> <div class="row"> <div class="col-5 d-none d-md-block"> <h3 class="text-center subtitle fancy"><span><span class="line-left"></span>Internet<span class="line-right"></span></span></h3> </div> <div class="col-2 d-none d-md-block"> <h3 class="text-center subtitle fancy"><span class="line-down">Phone</span></h3> </div> <div class="col-5 d-none d-md-block"> <h3 class="text-center subtitle fancy"><span><span class="line-left"></span>Security<span class="line-right"></span></span></h3> </div> <div class="col-12 col-md-3 mb-2 mb-md-0 card-wrapper"> <div class="card phone-card2"> <div class="card-body"> <h3 class="card-title">Unlimited Business Service</h3> <p class="card-text">7MB Plans start at $99.99/mo*</p> <a href="./business/basic/" class="btn btn-primary">Learn More</a> </div> </div> </div> <div class="col-12 col-md-3 mb-2 mb-md-0 card-wrapper"> <div class="card phone-card2"> <div class="card-body"> <h3 class="card-title">Metered Business Service</h3> <p class="card-text">10MB Plans start at $59.99/mo.</p> <a href="./business/metered/" class="btn btn-primary">Learn More</a> </div> </div> </div> <div class="col-12 col-md-3 mb-2 mb-md-0 card-wrapper"> <div class="card phone-card2"> <div class="card-body"> <h3 class="card-title">Unlimited Calling Business Phone</h3> <p class="card-text">Only $19.99/mo. per line (up to 20 lines)</p> <a href="./business/phone/" class="btn btn-primary">Learn More</a> </div> </div> </div> <div class="col-12 col-md-3 mb-2 mb-md-0 card-wrapper"> <div class="card phone-card2"> <div class="card-body"> <h3 class="card-title">Video Surveillance</h3> <p class="card-text">$Price on Consultation*</p> <a href="./business/cameras/" class="btn btn-primary">Learn More</a> </div> </div> </div> <div class="col-12 col-md-3 mb-2 mb-md-0 card-wrapper"> <div class="card phone-card2"> <div class="card-body"> <h3 class="card-title">Cyber Security</h3> <p class="card-text">$Price on Consultation*</p> <a href="./business/cyber-security/" class="btn btn-primary">Learn More</a> </div> </div> </div> </div> <div class="row"> <div class="col-12"> <p class="mt-4"></p> </div> </div> <div class="row"> <div class="col-12 mt-1"> <div class="text-center"><a href="./check/" class="btn btn-secondary">Check Coverage</a></div> </div> </div> </div> </div> <div class="pt-4 pb-3"> <div class="container"> <div class="row mt-3"> <a id="connections" class="anchor"></a> <div class="col-12 text-left mb-4"><h2><i class="fa fa-home" aria-hidden="true"></i> Digital Connections</h2></div> <div class="col-12 col-md-3 mb-2 mb-md-0"> <div class="card phone-card"> <img class="card-img-top" :src="$withBase('directtv.png')" alt=""> <div class="card-body"> <h3 class="card-title">DIRECT TV</h3> <p class="card-text">Watch live TV and up to 45,000 shows and movies, on demand and on up to 5 devices at once!</p> <a href="./direct-tv/" class="btn btn-primary">About Direct TV</a> </div> </div> </div> <div class="col-12 col-md-3 mb-2 mb-md-0"> <div class="card phone-card"> <img class="card-img-top" :src="$withBase('att-1.png')" alt=""> <div class="card-body"> <h3 class="card-title">AT&amp;T Wireless</h3> <p class="card-text">The Mid-Ohio Valley's Only AT&amp;T Preferred Dealer * Unlimited is just the beginning! </p> <a href="./attwireless/" class="btn btn-primary">About AT&amp;T Wireless</a> </div> </div> </div> <div class="col-12 col-md-3 mb-2 mb-md-0"> <div class="card phone-card"> <img class="card-img-top" :src="$withBase('google-1.png')" alt=""> <div class="card-body"> <h3 class="card-title">Google Home WiFi</h3> <p class="card-text">Google Wifi points work together to create a connected system that gives you a strong signal throughout your home.  </p> <a href="./google/" class="btn btn-primary">About Google Wifi</a> </div> </div> </div> <div class="col-12 col-md-3 mb-2 mb-md-0"> <div class="card phone-card"> <img class="card-img-top" :src="$withBase('ooma.png')" alt=""> <div class="card-body"> <h3 class="card-title">Ooma Home Phone</h3> <p class="card-text">Get all the bells and whistles, like the ability to block telemarketers and other unwanted callers so they can’t bother you.</p> <a href="./phone/" class="btn btn-primary">About Ooma</a> </div> </div> </div> </div> </div></div>

<div class="packages-background pb-5">
<div class="container">
<div class="row mt-5 pt-4">
<a id="testimonials" class="anchor"></a>
<div class="col-12 mb-3 text-left"><h2><i class="fa fa-2x fa-comments-o"></i> Testimonials</h2></div>
<div class="col-12 col-md-4 mb-2 mb-md-0">
<div class="card testimonial">
<div class="card-body text-center">
<div class="testimonial-img-wrapper"><div class="testimonial-img" style="background-image: url('./barrett.jpg');"></div></div>
<p class="card-text mt-3">We have had New Era Broadband for our internet since the beginning of the company. We have had excellent service and watched it expand coverage area. I highly recommend New Era Broadband.</p>
<h3 class="card-title">Marcia Barrett</h3>
</div>
</div>
</div>
<div class="col-12 col-md-4 mt-0 mb-2 mb-md-0">
<div class="card testimonial">
<div class="card-body text-center">
<div class="testimonial-img-wrapper"><div class="testimonial-img" style="background-image: url('./Amberger.jpg');"></div></div>
<p class="card-text mt-3">We have been subscribed with New Era Broadband for several years now.  We are very pleased with the service we've received, like the variety of speeds to choose from and the prompt response the few times we've needed assistance. Thanks New Era for our rural high speed internet!!</p>
<h3 class="card-title">Bill Amberger</h3>
</div>
</div>
</div>
<div class="col-12 col-md-4 mb-2 mb-md-0">
<div class="card testimonial">
<div class="card-body text-center">
<div class="testimonial-img-wrapper"><div class="testimonial-img" style="background-image: url('./hunter.jpg');"></div></div>
<p class="card-text mt-3">New Era has been a life saver for my husband!!  He can finish his office work at home now rather than spending the extra hours at the office.  Thank you New Era Broadband for providing professional service to our rural area!!</p>
<h3 class="card-title">Tonja Hunter</h3>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="container">
<div class="row mt-5 mb-4">
<div class="col-12 mb-3">
<a id="contact" class="anchor"></a>
<h2 class="mb-3 d-inline-block"><i class="fa fa-comment"></i> Contact Us</h2>
<p>Our helpful and professional staff are available to assist you during the listed hours (excluding holidays) and when we're not available, our friendly answering service will be happy to take your calls! <a href="./support">Contact our team</a> to get started today!</p>
</div>
<div class="col-12 col-md-4 offset-0 offset-md-2">
<h3>Customer Support</h3>
<em>Billing and account questions</em>
<p>Monday - Friday 9 a.m. - 5 p.m.</p>
<p>24/7 Phone Message System</p>
<p>Call: (740) 992-0620 (24/7)</p>
</div>
<div class="col-12 col-md-4 offset-0 offset-md-2">
<h3>Technical Support</h3>
<em>Interruption in services</em>
<p>Monday - Friday 9 a.m. - 5 p.m.</p>
<p>24/7 Phone Message System</p>
<p>Call: (740) 992-0620 (24/7)</p>
</div>
</div>
</div>
<div class="packages-background">
<div class="container mb-4 pt-4 pb-4">
<div class="row">
<a id="about" class="anchor"></a>
<div class="col-12 mb-3"><h2>About New Era Broadband</h2></div>
<div class="col-12">
<p>New Era Broadband, LLC, operating as New Era Broadband Services is a registered LLC in the State of Ohio and the County of Meigs. We were formed out of the need to provide high speed and broadband Internet access to the under-served and unserved rural customers in Meigs County, the Mid-Ohio Valley and beyond.</p>
<p>The management of New Era Broadband Services is committed to providing the highest quality services at the most reasonable price possible. Our goal is to treat our customers exactly how we hope to be treated ourselves. All employees are held to the highest standards possible, and will treat you with respect and courtesy.</p>
<p>We are in business to earn a reasonable profit by providing services that are needed and in demand, and deliver them in a quality manner. If you have any questions or remarks about New Era Broadband, LLC, or the products and services we offer, simply contact us at (740) 992-0620. We are eager to hear what you have to say.</p>
</div>
</div>
</div>
</div>
<div class="container">
<div class="row">
<div class="col-12 text-center">
<p style="color: rgb(172, 172, 172);">Members of the Wireless Internet<br>Service Providers Association<br><a href="http://www.wispa.org/"><img style="width: 90px;" :src="$withBase('wispa.png')" alt=""></a></p>
</div>
</div>
</div> <!-- .container -->