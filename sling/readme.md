---
title: Sling TV at New Era Broadband
meta:
- name: description
  content: Sling TV provided by New Era Broadband.

---
# Sling TV

<img src="https://www.newerabroadband.com/sling-devices.jpg">

<img class="d-none d-md-block" src="https://www.newerabroadband.com/icons-desktop.png">

<img class="d-block d-md-none" src="https://www.newerabroadband.com/icons-tablet.jpg">

<img src="https://www.newerabroadband.com/local-antenna2.jpg">

* **LONG RANGE HD RECEPTION:** This yagi antenna can capture 1080p HDTV reception and 32db high gain reception within a 150-mile range | Receive free digital broadcast High Definition TV signals, making this the best long range TV antenna. Reception: VHF/UHF/FM | Reception range: 150 miles | Dual TV Outputs | Easy Installation | High Sensitivity Reception | Built-in Super Low Noise Amplifier | Power : AC15V 300mA
* **BUILT-IN FEATURES:** Our digital TV antenna for HDTV includes a built-in 360 degree motor rotor to allow you to find the best reception. Additionally, our outdoor TV antenna includes a built-in super low noise amplifier.Working Frequency: VHF 40\~300MHz | UHF 470\~890MHz.
* **WEATHER RESISTANT:** This TV antenna is fully functioning in the outdoors and can handle any type of weather pattern, making it one of the best antennas for HDTV reception.
* **CONVENIENT:** This long range TV antenna contains a wireless remote control to adjust the antenna from inside the comfort of your home. This yagi antenna also features dual TV outputs to maximize utility. Wireless remote controller.

*The actual device may differ slightly in appearance form the image displayed*